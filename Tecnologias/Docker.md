# **Docker**

## **Sumario**

- [Até chegar no docker](#ate-chegar-no-docker)
- [O que é container](#o-que-e-container)
- [Vantagens dos containers](#vantagens-dos-containers)
- [O que é docker](#o-que-e-docker)
- [Estados de um container](#estados-de-um-container)
- [Camadas de Filesystem](#camadas-de-filesystem)
- [Dockerfile](#dockerfile)
- [Docker compose](#docker-compose)
- [Comandos docker](#comandos-docker)

---

### **`Ate chegar no docker`**

1. Sistemas eram colocados em maquinas fisicas.
    - Problemas:
        - Custo de rede
        - Custo de eletricidade
        - Manutenção
        - Segurança
        - Atualização de sistemas
        - Não tira proveito 100% do servidor
            - Ociosidade
            - Recursos disperdiçados

2. Virtualização
    - Vantagens:
        - Custo baixo de hardware
        - Baixa Ociosidade
        - Melhor utilização de recursos
    - Problemas:
        - Uma maquina virutal tem um sistema Operacional
        - Varios sistemas operacionais = mais recursos usados
        - Manutenção
        - Segurança
        - Atualização de sistemas

---

### **`O que e Container`**

- Um container é um ambiente isolado contido em um servidor para empacotar aplicações, e são executados a partir de uma imagem
---

### **`Vantagens dos containers`**

- Containers dividem recursos de um mesmo sistema operacional
- Mais leve que uma maquina virtual
- Se um container cair não afeta os outros containers
- Isolar aplicações com suas caracteristicas
- Melhor controle de recursos( cpu, rede, disco )
- Agilidade na criação e remoção de containers
- Facilidade na hora de trabalhar com versões das aplicações, linguagens ...

---

### **`O que e docker`**

- Docker Inc -> Empresa de PaaS (Platform as a Service)
- Docker é uma plataforma open source que facilita a criação e administração de containers.
- Tecnologias Docker:
    - Docker compose
    - Docker swarm
    - Docker hub
    - Docker Machine

---

### **`Estados de um container`**

- RUNNING
- STOPPED

---

### **`Camadas de Filesystem`**

- Toda imagem é baixada em Camadas e elas podem ser reaproveitadas
    - Exemplo:
        - Ubuntu
            - Camada 1 : 1233535...86786453 - Igual
            - Camada 2 : 8678674...55728298 - Igual
            - Camada 3 : 5703567...47269873
            - Camada 4 : 6986789...54875454
        - CentOs
            - Camada 1 : 1233535...86786453 - Igual
            - Camada 2 : 8678674...55728298 - Igual
            - Camada 3 : 0098885...23474858
            - Canada 4 : 9888549...58576848
- Camadas Base são READ ONLY
- Camadas de READ/WRITE ficam acima da imagem Base

---

### **`Dockerfile`**

- Arquivo para criar uma imagem docker

---

### **`Docker compose`**

- Arquivo para definir e configurar multiplos containers

---

### **`Comandos Docker`**

- docker ps ...
- docker images ...
- docker run ...
- docker stop ...
- docker rm ...
- docker rmi ...
- docker volume ...
- docker container ...
- docker network ...

---

> **Author** : Thiago Hayaci

> **Email** : tyhayaci@gmail.com
