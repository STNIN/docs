#!/bin/sh

# ========== ========== FIRST STEPS ========== ==========

read -p "Name to Config: " NAME

# ========== SSH ==========

rm -rf ~/.ssh
echo "SSH File Deleted\n"

# ========== GPG ==========

gpg --delete-key $NAME
gpg --delete-secret-key $NAME
echo "\nGPG Key Deleted\n"

# ========== GIT ==========

rm -rf ~/.gitconfig
echo "GIT File Deleted\n"
