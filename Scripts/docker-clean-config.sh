#!/bin/sh

echo "alias dockerrmi='docker rmi \$(docker images -q)'" >> ~/.bashrc
echo "alias dockerstp='docker stop \$(docker ps -a -q)'" >> ~/.bashrc
echo "alias dockerrmp='docker rm \$(docker ps -a -q)'" >> ~/.bashrc
echo "alias dockervp='docker volume prune'\n" >> ~/.bashrc

echo "dockerclean(){" >> ~/.bashrc
echo "	dockerrmi" >> ~/.bashrc
echo "	dockerstp" >> ~/.bashrc
echo "	dockerrmp" >> ~/.bashrc
echo "	dockervp" >> ~/.bashrc
echo "}" >> ~/.bashrc

echo "function prune_docker(){"  >> ~/.bashrc
echo "	docker rm $(docker ps -aq)" >> ~/.bashrc
echo "	docker rmi $(docker images -aq)" >> ~/.bashrc
echo "	docker system prune" >> ~/.bashrc
echo "	docker volume prune" >> ~/.bashrc
echo "	docker network prune" >> ~/.bashrc
echo "	docker image prune" >> ~/.bashrc
echo "}" >> ~/.bashrc
