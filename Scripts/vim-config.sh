#!/bin/sh

echo "set number" >> /etc/vim/vimrc
echo "set mouse=a" >> /etc/vim/vimrc
echo "syntax on" >> /etc/vim/vimrc
echo "set showmatch" >> /etc/vim/vimrc
echo "set autoread" >> /etc/vim/vimrc
echo "set ruler" >> /etc/vim/vimrc
echo "set hlsearch" >> /etc/vim/vimrc
echo "set incsearch" >> /etc/vim/vimrc
echo "set showcmd" >> /etc/vim/vimrc
echo "set complete+=kspell" >> /etc/vim/vimrc
echo "set tabstop=4" >> /etc/vim/vimrc
echo "set autoindent" >> /etc/vim/vimrc
echo "set expandtab" >> /etc/vim/vimrc
echo "set cursorline" >> /etc/vim/vimrc
echo "set laststatus=2" >> /etc/vim/vimrc
echo "set wildmenu" >> /etc/vim/vimrc
echo "set title" >> /etc/vim/vimrc
echo "set confirm" >> /etc/vim/vimrc

