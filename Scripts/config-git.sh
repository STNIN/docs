#!/bin/sh

# ========== ========== FIRST STEPS ========== ==========
echo "\033[1;37m"

read -p "Name to Config: " NAME
read -p "Email to Config: " EMAIL

# ========== ========== GIT ========== ==========

echo "\033[1;37mStarting Config Git .....\n\033[1;33m"

git --version

read -p "TO CONFIG SSH PRESS [ENTER]" SSH

# ========== SSH ==========
echo "\n\n\033[1;34mSetting Ssh .....\n"

ssh-keygen -t ed25519 -C $EMAIL

echo "\n\033[1;37m.ssh/*.pub >> \n"

cat ~/.ssh/*.pub

echo "\n\033[1;32mTake all Information Received for file Above '.ssh/*.pub' and put in SSH GITLAB or ... \n\033[1;33m"

read -p "TO CONFIG GPG PRESS [ENTER]" GPG

# ========== GPG ==========
echo "\n\n\033[1;34mSetting Gpg .....\n"

echo "\033[1;32mKEY: 1, default\nKEY VALUE: 4096\nEXPIRE: 0, does not expire\nNAME: GITLAB or ...\nEMAIL: GITLAB or ...\nPASSWORD: Choose One\n\033[1;34m"

gpg --full-gen-key

echo "\033[1;37mgpg armor >> \n"

gpg --armor --export $EMAIL

echo "\n\033[1;32mTake >>all<< Information Received for file Above 'gpg armor' and put in SSH GITLAB or ... \n\033[1;33m"

read -p "TO CONTINUE PRESS [ENTER]" CONTINUE

# ========== GIT ==========
echo "\n\033[1;34mSetting Git .....\n"

gpg --list-secret-keys

echo "\033[1;33m"
read -p "Copy Hash Above and Send to Config: " HASH

git config --global user.name $NAME
git config --global user.email $EMAIL
git config --global user.signingkey $HASH
git config --global commit.gpgsign true

echo "\033[1;37m"

git config --list

echo "\n\033[1;32mFinished Config, Please LOGOUT or REBOOT\n\n"
