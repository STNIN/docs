#!/bin/sh

echo "Install Programs\n"
read -p "To start press [enter]" INSTALL

echo "Updating ...\n"
sudo apt update -y
echo "Upgrading ...\n"
sudo apt upgrade -y

install_complete_percent=100
quantity_programs=13
porcent_step=$((install_complete_percent/quantity_programs))
atual_porcent=$porcent_step

reset
#1 - HTOP
echo "Installing htop :: ${atual_porcent}%\n"
sudo apt install -y htop
atual_porcent=atual_porcent + porcent_step

reset
#2 - CMAKE
echo "Installing cmake :: ${atual_porcent}%\n"
sudo apt install -y cmake
atual_porcent=atual_porcent + porcent_step

reset
#3 - GCC-MULTILIB
echo "Intalling gcc-multilib :: ${atual_porcent}%\n"
sudo apt install -y gcc-multilib
atual_porcent=atual_porcent + porcent_step

reset
#4 - GIT
echo "Intalling git :: ${atual_porcent}%\n"
sudo apt install -y git
atual_porcent=atual_porcent + porcent_step

reset
#5 - VALGRIND
echo "Intalling valgrind :: ${atual_porcent}%\n"
sudo apt install -y valgrind
atual_porcent=atual_porcent + porcent_step

reset
#6 - DOCKER
echo "Intalling docker :: ${atual_porcent}%\n"
sudo apt install -y docker
echo "Configuring docker permissions ...\n"
sudo usermod -aG docker $USER
atual_porcent=atual_porcent + porcent_step

reset
#7 - DOCKER-COMPOSE
echo "Intalling docker-compose :: ${atual_porcent}%\n"
sudo apt install -y docker-compose
atual_porcent=atual_porcent + porcent_step

reset
#8 - BUILD-ESSENTIAL
echo "Intalling build-essential :: ${atual_porcent}%\n"
sudo apt install -y build-essential
atual_porcent=atual_porcent + porcent_step

reset
#9 - GCC
echo "Intalling gcc :: ${atual_porcent}%\n"
sudo apt install -y gcc
atual_porcent=atual_porcent + porcent_step

reset
#10 - G++
echo "Intalling g++ :: ${atual_porcent}%\n"
sudo apt install -y g++
atual_porcent=atual_porcent + porcent_step

reset
#11 - VIM
echo "Intalling vim :: ${atual_porcent}%\n"
sudo apt install -y vim
atual_porcent=atual_porcent + porcent_step

reset
#12 - MICROK8S
echo "Intalling micro-k8s :: ${atual_porcent}%\n"
sudo snap install microk8s --classic
echo "MicroK8s status ...\n"
microk8s.status
echo "MicroK8s permission configuring ...\n"
sudo usermod -aG microk8s $USER
echo "Creating Folder .kube ...\n"
mkdir ~/.kube
echo "Configuring permissions .kube ...\n"
sudo chown -fR $USER ~/.kube
echo "Config microk8s ...\n"
microk8s.kubectl config view --raw > $HOME/.kube/config
atual_porcent=atual_porcent + porcent_step

reset
#13 - KUBECTL
echo "Intalling kubectl :: ${atual_porcent}%\n"
sudo snap install kubectl --classic
echo "Configuring alias microk8s ...\n"
sudo snap alias microk8s.kubectl kubectl
echo "Kubectl namespaces ...\n"
kubectl get namespace
echo "Kubectl Version ...\n"
kubectl version
echo "To run dashboard :: --> microk8s.dashboard-proxy <-- \n"
atual_porcent=atual_porcent + porcent_step

difference=$((install_complete_percent - atual_porcent))
atual_porcent=$((atual_porcent + difference))

echo "Installation Complete :: ${atual_porcent}\n"
